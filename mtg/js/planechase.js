var planes = [];
var usedPlanes=[];
$(document).ready(function() {
    getPlanes();
    $("#draw").click(function() {
        drawPlane();
    });
    $("#roll").click(function() {
        rollDie();
    });
    $("#shuffle").click(function() {
        $("#shuffle, #draw, #roll").attr("disabled", true);
        getPlanes();
    });
});

function drawPlane() {
    $("#active-plane").fadeOut("fast");
    $("#shuffle, #draw, #roll").attr("disabled", true);

    if(planes.length < 1) {
        getPlanes();
    }

    var cardId = planes.pop();
    $.get("https://api.magicthegathering.io/v1/cards/"+cardId, function(data) {
        if(data.card) {
            $("#active-plane").attr("src", data.card.imageUrl);
            setTimeout(function() {
                $("#active-plane").fadeIn();
            }, 200);
            $("#shuffle, #draw, #roll").removeAttr("disabled");
        }
    }).fail(function() {
        drawPlane();
    });
}

function rollDie() {
    $("#shuffle, #draw, #roll").attr("disabled", true);

    var possibilities = ["PLANESWALK","VITAL","JESUS","JUNY","GERSON","CHAOS"];
    var index = Math.floor((Math.random() * 6));
    var currentplane = $("#active-plane").attr("src");

    var $planeContainer = $("#active-plane").parent();
    $("#roll-result").text(possibilities[index]);
    $("#active-plane").fadeOut(400, function() {
        $("#roll-result").fadeIn();
    });

    setTimeout(function() {
        $("#roll-result").fadeOut(400, function() {
             $("#active-plane").fadeIn();
             $("#shuffle, #draw, #roll").removeAttr("disabled");
        });
    }, 1500);
}

function shuffle(array) {
    $("#shuffle").attr("disabled", true);
    var currentIndex = array.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}

function getPlanes() {
    planes=[];
    $.get("https://api.magicthegathering.io/v1/cards?types=Plane", function(data) {
        if(data.cards.length > 0) {
            for(var i=0; i<data.cards.length; i++) {
                planes.push(data.cards[i].multiverseid);
            }
            planes = shuffle(planes);
            $("#shuffle, #draw, #roll").removeAttr("disabled");
        }
    });
}