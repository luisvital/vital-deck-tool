var users = [];

/***Initialize Firebase***/
// firebase.database().ref('Users').once('value').then(function(dataSnapshot) {
//     console.log(dataSnapshot.val());

//     users = Object.keys(dataSnapshot.val());
//     $("#name").autocomplete({
//         source: function(request, response) {
//             var result = $.ui.autocomplete.filter(users, request.term);
//             response(result);
//         }
//     });
//     $("#name").autocomplete("widget").insertAfter($("#user-modal .modal-title").parent());
// });
/**End Firebase **/

$(document).ready(function() {

    //window.location.href = 'cards';

    //show modal if user not logged in
    // if(sessionStorage.getItem('user') == null) {
    //     $("#user-modal").modal("show");
    // }
    // else {
    //     $("#welcome-message").text("Welcome " + sessionStorage.getItem('user'));
    // }

    //set user
    $("#set-user").click(function() {

        //if new user save to db
        if(users.indexOf($("#name").val()) < 0) {
            firebase.database().ref('Users/' + $("#name").val()).set({
                Cards: "{}"
            });
        }

        //save username to session
        sessionStorage.setItem('user', $("#name").val());
        $("#user-modal").modal("hide");
    });
});