$(document).ready(function() {
    $("#cards").hide();
    $.ajaxSetup({
        async: false
    });

    $(".scroll-bottom").click(function() {
        $("html, body").animate({ scrollTop: $(document).height()});
    });
    $(".scroll-top").click(function() {
        $("html, body").animate({ scrollTop: 0 });
    });

    $.get("https://api.magicthegathering.io/v1/sets", function(data) {
        var template = Handlebars.compile($("#mtg-sets-template").html());
        var html = template({set: data.sets});
        $("#sets").append(html);

        $(".symbol").each(function(index) {
            var code = $(this).parent().attr("id");
            $(this).html("<img src=\"../img/"+code+".png\" class='img-thumbnail' onError=\"this.src='../img/blank.png'; this.className='';\"/>");
        });
    });
});

function getCardsFromSet(set) {
    $("#loading-modal").modal("show");
    var cards = [];
    var page = 1;
    while(page > 0) {
        $.get("https://api.magicthegathering.io/v1/cards?set="+set+"&page="+page, function(data) {
            if(data.cards.length > 0) {
                cards = cards.concat(data.cards);
                page++;
            }
            else {
                page = -1;
            }
        });
    }

    $("#sets").hide();
    var template = Handlebars.compile($("#mtg-card-template").html());
    var html = template({cards: cards});
    $("#cards").append(html);
    $("#cards").show();
    $("#loading-modal").modal("hide");
}

function showSets() {
    $("#cards").hide();
    $("#card-data").remove();
    $("#sets").show();
}